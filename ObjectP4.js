//function pairs(obj) {
// Convert an object into a list of [key, value] pairs.
// http://underscorejs.org/#pairs
//}

//function pairs(obj) {
// Convert an object into a list of [key, value] pairs.
// http://underscorejs.org/#pairs



const testObject = {
  name: 'Bruce Wayne', age: 36, location: 'Gotham', func: function () {

  }
}; // use this object to test your functions

function pairs(obj) {
  let newArr = [];
  for (key in obj) {
    if (obj[key].constructor === Function) {
      continue;
    } else
      newArr.push([key, obj[key]]);
  }
  return newArr;
}
