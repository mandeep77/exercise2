//function values(obj) {
// Return all of the values of the object's own properties.
// Ignore functions
// http://underscorejs.org/#values
//}

const testObject = {
    name: 'Bruce Wayne', age: 36, location: 'Gotham', func: function () {

    }
}; // use this object to test your functions

function values(obj) {
    let newArr = [];
    for (key in obj) {
        if (obj[key].constructor === Function) {
            continue;
        } else
            newArr.push(obj[key]);
    }
    return newArr;
}
