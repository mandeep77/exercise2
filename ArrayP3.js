// Problem 2 for map method.

function each(array, callback) {
    for (let i = 0; i < array.length; i++) {
        let element = array[i];
        callback(element, i, array);
    }
}

function reduce(array, callback, startingValue) {
    if (array == undefined || callback == undefined) {
        return "Please Provide an array, callback function and element to be found"
    }
    each(array, function (element) {
        if (startingValue == undefined) {
            return startingValue = element;
        }
        else {
            return startingValue = callback(startingValue, element);
        }

    });;

    return startingValue;
}
function callback(num1, num2) {
    return num1 + num2;
};

module.exports = reduce;