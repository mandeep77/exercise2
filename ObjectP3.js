//function mapObject(obj, cb) {
// Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
// http://underscorejs.org/#mapObject
//}

var myObject = { 'a': 1, 'b': 2, 'c': 3 };

function map(obj, cb) {
  if (obj == undefined || cb == undefined) {
    return [];
  }
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      cb(key, obj);
    }
  }
  return obj
}

function cb(key, obj) {
  return obj[key] *= 2;

}

console.log(map(myObject,cb));