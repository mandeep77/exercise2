// Problem1 forEach


function each(array, callback) {
    if (array == undefined || callback == undefined) {
       return console.log("Please Provide an array, callback function and element to be found")
    }
    for (let i = 0; i < array.length; i++) {
        let element = array[i];
        callback(element, i, array);
    }
}

function callback(element, i, array) {
    return (element, i, array);
}

//each(items,callback);

module.exports = each;


