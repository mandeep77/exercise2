//function defaults(obj, defaultProps) {
// Fill in undefined properties that match properties on the `defaultProps` parameter object.
// Return `obj`.
// http://underscorejs.org/#defaults
//}



function defaults(obj, defaultProps) {
    if (obj == undefined || defaultProps == undefined) {
        return {};
    }

    for (let key in obj) {
        for (let key1 in defaultProps) {
            if (key == key1) {
                continue; 
            }
            else {
                obj[key1] = defaultProps[key1];
            }

        }
    }
    return obj;
}


