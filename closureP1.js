//function counterFactory() {
    // Return an object that has two methods called `increment` and `decrement`.
    // `increment` should increment a counter variable in closure scope and return it.
    // `decrement` should decrement the counter variable and return it.
  //}
  function counterFactory() {

    let current = 0;

    let increment = plus => {

        current += plus;
        return current;
    };

    let decrement = minus=> {

        current -= minus;
        return current;
    }

    return {increment: increment,
            decrement: decrement};
}

//let myFactory = counterFactory();

//console.log(myFactory.decrement(50))
