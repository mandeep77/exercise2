
//Problem 6 for flatten method.
   let flat =[];
	function flatten(array) {
		

        if (array == undefined) {
            return "Please Provide an array, callback function and element to be found"
        }
		for (let i = 0; i < array.length; i++) {
			if (array[i].constructor === Array) {
					flatten(array[i]);
			} else {
				flat.push(array[i]);
			}
		} 
        return flat;
	}

    module.exports = flatten;