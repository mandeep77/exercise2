// Problem 5 for filter method.

function each(array, callback) {
    for (let i = 0; i < array.length; i++) {
        let element = array[i];
        callback(element, i, array);
    }
}

function filter(array, callback) {
    if (array == undefined || callback == undefined) {
        return "Please Provide an array and callback function!"
    }
    let result =  [];
    each(array, function (element) {
        if(callback(element)){
            result.push(element);
        }
                    
    });

    return result;
};
function callback(element) {
    if (element>5) {
        return true}
};




module.exports = filter;