// Problem 4 for find method


function find(array, callback) {
    if (array == undefined || callback == undefined) {
        return "Please Provide an array, callback function and element to be found"
    }
    
    for(let i =0;i<array.length;i++) {
        let result = array[i];
        let find = callback(result);
        if(find){
          return result;
        }      
    };
}




module.exports = find;