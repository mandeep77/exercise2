// Problem 2 for map method.

function each(array, callback) {
    for (let i = 0; i < array.length; i++) {
        let element = array[i];
        callback(element, i, array);
    }
}

function map(array, callback) {
    if (array == undefined || callback == undefined) {
        return "Please Provide an array, callback function and element to be found"
    }
    let mapped = [];
    each(array, function (elem, i, arr) {
        var result = callback(elem, i, arr);
        mapped.push(result);
    });;

    return mapped;
}
function callback(ele, i, arr) {
    return ele * i;
};

module.exports = map;